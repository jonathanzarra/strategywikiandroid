package org.strategywiki.feed.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import org.strategywiki.feed.FeedCoordinatorBase;
import org.strategywiki.feed.announcement.AnnouncementCardView;
import org.strategywiki.feed.featured.FeaturedArticleCardView;
import org.strategywiki.feed.image.FeaturedImageCardView;
import org.strategywiki.feed.model.Card;
import org.strategywiki.feed.model.CardType;
import org.strategywiki.feed.news.NewsListCardView;
import org.strategywiki.feed.offline.OfflineCardView;
import org.strategywiki.feed.searchbar.SearchCardView;
import org.strategywiki.views.DefaultRecyclerAdapter;
import org.strategywiki.views.DefaultViewHolder;
import org.strategywiki.views.ItemTouchHelperSwipeAdapter;

public class FeedAdapter<T extends View & FeedCardView<?>> extends DefaultRecyclerAdapter<Card, T> {
    public interface Callback extends ItemTouchHelperSwipeAdapter.Callback,
            ListCardItemView.Callback, CardHeaderView.Callback,
            FeaturedImageCardView.Callback, SearchCardView.Callback, NewsListCardView.Callback,
            AnnouncementCardView.Callback, FeaturedArticleCardView.Callback {
        void onShowCard(@Nullable Card card);
        void onRequestMore();
        void onRetryFromOffline();
        void onError(@NonNull Throwable t);
    }

    @NonNull private FeedCoordinatorBase coordinator;
    @Nullable private Callback callback;

    public FeedAdapter(@NonNull FeedCoordinatorBase coordinator, @Nullable Callback callback) {
        super(coordinator.getCards());
        this.coordinator = coordinator;
        this.callback = callback;
    }

    @Override public DefaultViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DefaultViewHolder<>(newView(parent.getContext(), viewType));
    }

    @Override public void onBindViewHolder(DefaultViewHolder<T> holder, int position) {
        Card item = item(position);
        T view = holder.getView();

        if (coordinator.finished()
                && position == getItemCount() - 1
                && callback != null) {
            callback.onRequestMore();
        }

        //noinspection unchecked
        ((FeedCardView<Card>) view).setCard(item);

        if (view instanceof OfflineCardView && position == 1) {
            ((OfflineCardView) view).setTopPadding();
        }
    }

    @Override public void onViewAttachedToWindow(DefaultViewHolder<T> holder) {
        super.onViewAttachedToWindow(holder);
        holder.getView().setCallback(callback);
        if (callback != null) {
            callback.onShowCard(holder.getView().getCard());
        }
    }

    @Override public void onViewDetachedFromWindow(DefaultViewHolder<T> holder) {
        holder.getView().setCallback(null);
        super.onViewDetachedFromWindow(holder);
    }

    @Override public int getItemViewType(int position) {
        return item(position).type().code();
    }

    public int getItemPosition(@NonNull Card card) {
        return items().indexOf(card);
    }

    @NonNull private T newView(@NonNull Context context, int viewType) {
        //noinspection unchecked
        return (T) CardType.of(viewType).newView(context);
    }
}
