package org.strategywiki.feed;

import android.content.Context;
import android.support.annotation.NonNull;

import org.strategywiki.billing.BillingManager;
import org.strategywiki.feed.ads.AdsClient;
import org.strategywiki.feed.aggregated.AggregatedFeedContentClient;
import org.strategywiki.feed.announcement.AnnouncementClient;
import org.strategywiki.feed.becauseyouread.BecauseYouReadClient;
import org.strategywiki.feed.continuereading.ContinueReadingClient;
import org.strategywiki.feed.mainpage.MainPageClient;
import org.strategywiki.feed.random.RandomClient;
import org.strategywiki.feed.searchbar.SearchClient;
import android.util.Log;

class FeedCoordinator extends FeedCoordinatorBase {
    Context context;

    FeedCoordinator(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void buildScript(int age) {
        if (age == 0) {
            addPendingClient(new SearchClient());
            addPendingClient(new AnnouncementClient());
        }
        addPendingClient(new AggregatedFeedContentClient());
        addPendingClient(new ContinueReadingClient());
        if (age == 0) {
            addPendingClient(new MainPageClient());
        }
        addPendingClient(new BecauseYouReadClient());

        if (age == 0) {
            addPendingClient(new RandomClient());

            Log.e("purchase status", String.valueOf(BillingManager.getInstance().isPro(context)));
            if (!BillingManager.getInstance().isPro(context)) {
                addPendingClient(new AdsClient());
            }
        }
    }
}