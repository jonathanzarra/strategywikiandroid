package org.strategywiki.feed.progress;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.Card;
import org.strategywiki.feed.model.CardType;

public class ProgressCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.PROGRESS;
    }
}
