package org.strategywiki.feed.mostread;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.FeedPageSummary;
import org.strategywiki.json.annotations.Required;

import java.util.Date;
import java.util.List;

public final class MostReadArticles {
    @SuppressWarnings("unused,NullableProblems") @Required @NonNull private Date date;
    @SuppressWarnings("unused,NullableProblems") @Required @NonNull private List<FeedPageSummary> articles;

    @NonNull public Date date() {
        return date;
    }

    @NonNull public List<FeedPageSummary> articles() {
        return articles;
    }
}
