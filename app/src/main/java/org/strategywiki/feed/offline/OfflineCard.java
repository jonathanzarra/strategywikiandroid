package org.strategywiki.feed.offline;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.Card;
import org.strategywiki.feed.model.CardType;

public class OfflineCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.OFFLINE;
    }
}
