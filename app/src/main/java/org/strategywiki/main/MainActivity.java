package org.strategywiki.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.view.ActionMode;
import android.view.View;

import org.strategywiki.R;
import org.strategywiki.activity.SingleFragmentToolbarActivity;
import org.strategywiki.billing.BillingManager;
import org.strategywiki.billing.IabHelper;
import org.strategywiki.billing.IabResult;
import org.strategywiki.billing.Inventory;
import org.strategywiki.billing.Purchase;
import org.strategywiki.navtab.NavTab;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import static org.strategywiki.billing.BillingManager.SKU_12_MONTH;
import static org.strategywiki.billing.BillingManager.SKU_1_MONTH;
import static org.strategywiki.billing.BillingManager.SKU_6_MONTH;
import static org.strategywiki.billing.BillingManager.SKU_LIFETIME;

public class MainActivity extends SingleFragmentToolbarActivity<MainFragment>
        implements MainFragment.Callback, IabHelper.QueryInventoryFinishedListener {

    private IabHelper billingHelper;

    private Handler handler = new Handler();
    private WeakReference<MainActivity> activity = null;


    public static Intent newIntent(@NonNull Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override protected MainFragment createFragment() {
        return MainFragment.newInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkBillingStatus();
        handler = new Handler();
        activity = new WeakReference<>(this);
    }

    private void checkBillingStatus() {
        billingHelper = new IabHelper(this, BillingManager.PUBLIC_KEY);
        billingHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    billingCheckFailed();
                    return;
                }
                if (billingHelper == null) return;

                try {
                    billingHelper.queryInventoryAsync(true, null,
                            Arrays.asList(SKU_LIFETIME, SKU_1_MONTH, SKU_6_MONTH, SKU_12_MONTH),
                            MainActivity.this);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    billingCheckFailed();
                }
            }
        });
    }

    @Override
    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
        if (!result.isSuccess()) {
            billingCheckFailed();
            return;
        }
        BillingManager.getInstance().setSubscriptions(inventory.getskuMap());
        Purchase lifetime = inventory.getPurchase(SKU_LIFETIME);
        Purchase everyMonth = inventory.getPurchase(SKU_1_MONTH);
        Purchase every6Month = inventory.getPurchase(SKU_6_MONTH);
        Purchase every12Month = inventory.getPurchase(SKU_12_MONTH);

        if (lifetime != null || everyMonth != null || every6Month != null || every12Month != null){
            BillingManager.getInstance().setPro(true,MainActivity.this);
        } else {
            BillingManager.getInstance().setPro(false,MainActivity.this);
        }
        addFragment();
    }


    private void billingCheckFailed() {
        addFragment();
    }

    private void addFragment() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!isFragmentCreated()) {
                    addFragmentDelayed();
                }
            }
        });
    }

    @Override
    protected void addFragment(MainFragment fragment) {
        // No-Op
    }

    void addFragmentDelayed(){
        if(activity.get() != null) {
            activity.get().getSupportFragmentManager().beginTransaction().add(getContainerId(), createFragment()).commit();
        }
    }


    @Override
    public void onTabChanged(@NonNull NavTab tab) {
        if (tab.equals(NavTab.EXPLORE)) {
            getToolbarWordmark().setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle("");
        } else {
            getToolbarWordmark().setVisibility(View.GONE);
            getSupportActionBar().setTitle(tab.text());
        }
    }

    @Override
    public void onSearchOpen() {
        getToolbar().setVisibility(View.GONE);
        setStatusBarColor(android.R.color.black);
    }

    @Override
    public void onSearchClose(boolean shouldFinishActivity) {
        getToolbar().setVisibility(View.VISIBLE);
        setStatusBarColor(R.color.dark_green);
        if (shouldFinishActivity) {
            finish();
        }
    }

    @Override
    public void onSupportActionModeStarted(@NonNull ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        getFragment().setBottomNavVisible(false);
    }

    @Override
    public void onSupportActionModeFinished(@NonNull ActionMode mode) {
        super.onSupportActionModeFinished(mode);
        getFragment().setBottomNavVisible(true);
    }

    @NonNull
    @Override
    public View getOverflowMenuAnchor() {
        View view = getToolbar().findViewById(R.id.menu_overflow_button);
        return view == null ? getToolbar() : view;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getFragment().handleIntent(intent);
    }

    @Override
    public void onBackPressed() {
        if (getFragment().onBackPressed()) {
            return;
        }
        finish();
    }
}
