package org.strategywiki.dataclient.page;

import android.support.annotation.Nullable;

import org.strategywiki.dataclient.ServiceError;

/**
 * Represents a summary of a page, useful for page previews.
 */
public interface PageSummary {
    boolean hasError();
    @Nullable ServiceError getError();
    @Nullable String getTitle();
    @Nullable String getExtract();
    @Nullable String getThumbnailUrl();
}
