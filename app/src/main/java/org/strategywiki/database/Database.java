package org.strategywiki.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.strategywiki.WikipediaApp;
import org.strategywiki.edit.summaries.EditSummary;
import org.strategywiki.history.HistoryEntry;
import org.strategywiki.pageimages.PageImage;
import org.strategywiki.readinglist.database.ReadingListRow;
import org.strategywiki.readinglist.page.ReadingListPageRow;
import org.strategywiki.savedpages.SavedPage;
import org.strategywiki.search.RecentSearch;
import org.strategywiki.useroption.database.UserOptionRow;
import org.strategywiki.util.log.L;

public class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "wikipedia.db";
    private static final int DATABASE_VERSION = 16;

    private final DatabaseTable<?>[] databaseTables = {
            HistoryEntry.DATABASE_TABLE,
            PageImage.DATABASE_TABLE,
            RecentSearch.DATABASE_TABLE,
            SavedPage.DATABASE_TABLE,
            EditSummary.DATABASE_TABLE,

            // Order matters. UserOptionDatabaseTable has a dependency on
            // UserOptionHttpDatabaseTable table when upgrading so this table must appear before it.
            UserOptionRow.HTTP_DATABASE_TABLE,
            UserOptionRow.DATABASE_TABLE,

            ReadingListPageRow.DISK_DATABASE_TABLE,
            ReadingListPageRow.HTTP_DATABASE_TABLE,
            ReadingListPageRow.DATABASE_TABLE,

            ReadingListRow.DATABASE_TABLE
    };

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (DatabaseTable<?> table : databaseTables) {
            table.upgradeSchema(sqLiteDatabase, 0, DATABASE_VERSION);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int from, int to) {
        L.i("Upgrading from=" + from + " to=" + to);
        WikipediaApp.getInstance().putCrashReportProperty("fromDatabaseVersion", String.valueOf(from));
        for (DatabaseTable<?> table : databaseTables) {
            table.upgradeSchema(sqLiteDatabase, from, to);
        }
    }
}
