package org.strategywiki.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.strategywiki.richtext.URLSpanNoUnderline;
import org.strategywiki.test.TestParcelUtil;
import org.strategywiki.test.TestRunner;

@RunWith(TestRunner.class) public class URLSpanNoUnderlineTest {
    @Test public void testCtorParcel() throws Throwable {
        Parcelable subject = new URLSpanNoUnderline("url");
        TestParcelUtil.test(subject);
    }
}
